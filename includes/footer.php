
<footer id="footer" class="border-top-0">
				<div class="container py-4">
					<div class="row py-5">
                    <div class="col-md-3 mb-4 mb-lg-0">
                        <img src="img/demos/law-firm/logo-footer.png" class="responsive" alt="" style="width:inherit;">
							<!-- <a href="index.html" class="logo pr-0 pr-lg-3 mb-3">
								<img alt="Porto Website Template" src="img/demos/law-firm/logo-footer.png" class="opacity-7 top-auto bottom-10" height="33">
							</a>							 -->
						</div>
						<div class="col-md-3 mb-4 mb-lg-0">	
                        <h5 class="text-3 mb-3">Ribeiro & Miranda | Advocacia</h5>						
							<p class="mt-4 mb-1">Escritório fundado há quase duas décadas na capital do País, onde a experiência, aliada à prática e ao conhecimento garantem ao cliente o melhor atendimento, sem abrir mão da tecnologia como aliada na gestão de processos.</p>
							<!-- <p class="mb-0"><a href="#" class="btn-flat btn-xs text-color-light"><strong class="text-2">CONHEÇA MAIS</strong><i class="fas fa-angle-right p-relative top-1 pl-2"></i></a></p> -->
						</div>
						<div class="col-md-3 mb-4 mb-lg-0">
							<h5 class="text-3 mb-3">Contatos</h5>
							<ul class="list list-icons list-icons-lg">
								<li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i><p class="m-0">SCRN 716, Bloco F, Loja 42, Asa Norte Brasília-DF - CEP: 70770-660</p></li>
								<li class="mb-1"><i class="fab fa-whatsapp text-color-primary"></i><p class="m-0"><a href="https://api.whatsapp.com/send?phone=5561982121086&text=Ol%C3%A1%2C%20vim%20atrav%C3%A9s%20do%20site%20da%20Ribeiro e Miranda | Advocacia%2C%20e%20desejo%20algumas%20informa%C3%A7%C3%B5es." target="_blank">(61) 98212-1086</a></p></li>
                                <li class="mb-1"><i class="fas fa-phone text-color-primary"></i><p class="m-0"><a href="tel:6132023999" target="_blank">(61) 3202-3999</a></p></li>
								<li class="mb-1"><i class="far fa-envelope text-color-primary"></i><p class="m-0"><a href="mailto:contato@ribeiroemiranda.adv.br">contato@ribeiroemiranda.adv.br</a></p></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h5 class="text-3 mb-3">Redes Sociais</h5>
							<ul class="header-social-icons social-icons">
								<li class="social-icons-facebook"><a href="#" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-instagram"><a href="#" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
								<li class="social-icons-linkedin"><a href="#" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright footer-copyright-style-2">
					<div class="container">
						<div class="row" style="padding:5px;">
							<div class="col d-flex align-items-center justify-content-center">
								<p>© Copyright <?=date("Y");?>. Ribeiro e Miranda | Advocacia - All Rights Reserved. <a href="https://www.victorbraga.com.br" target="_blank">Desenvolvimento por: Victor Braga Design e Desenvolvimento</a></p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<script>var purecookieTitle="Nós valorizamos sua privacidade",purecookieDesc="Nosso website coleta informações do seu dispositivo e da sua navegação por meio de cookies para permitir funcionalidades como: melhorar o funcionamento técnico das páginas, mensurar a audiência do website e oferecer produtos e serviços relevantes por meio de anúncios personalizados. Para saber mais sobre as informações e cookies, acesse nossa ",purecookieLink='<a href="https://www.cssscript.com/privacy-policy/" target="_blank">Política de Privacidade.</a>',purecookieButton="Aceitar";function pureFadeIn(e,o){var i=document.getElementById(e);i.style.opacity=0,i.style.display=o||"block",function e(){var o=parseFloat(i.style.opacity);(o+=.02)>1||(i.style.opacity=o,requestAnimationFrame(e))}()}function pureFadeOut(e){var o=document.getElementById(e);o.style.opacity=1,function e(){(o.style.opacity-=.02)<0?o.style.display="none":requestAnimationFrame(e)}()}function setCookie(e,o,i){var t="";if(i){var n=new Date;n.setTime(n.getTime()+24*i*60*60*1e3),t="; expires="+n.toUTCString()}document.cookie=e+"="+(o||"")+t+"; path=/"}function getCookie(e){for(var o=e+"=",i=document.cookie.split(";"),t=0;t<i.length;t++){for(var n=i[t];" "==n.charAt(0);)n=n.substring(1,n.length);if(0==n.indexOf(o))return n.substring(o.length,n.length)}return null}function eraseCookie(e){document.cookie=e+"=; Max-Age=-99999999;"}function cookieConsent(){getCookie("purecookieDismiss")||(document.body.innerHTML+='<div class="cookieConsentContainer" id="cookieConsentContainer"><div class="cookieTitle"><a>'+purecookieTitle+'</a></div><div class="cookieDesc"><p>'+purecookieDesc+" "+purecookieLink+'</p></div><div class="cookieButton"><a onClick="purecookieDismiss();">'+purecookieButton+"</a></div></div>",pureFadeIn("cookieConsentContainer"))}function purecookieDismiss(){setCookie("purecookieDismiss","1",7),pureFadeOut("cookieConsentContainer")}window.onload=cookieConsent();</script>
		<!-- Vendor -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="vendor/popper/umd/popper.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/common/common.min.js"></script>
		<script src="vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="vendor/isotope/jquery.isotope.min.js"></script>
		<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="vendor/vide/jquery.vide.min.js"></script>
		<script src="vendor/vivus/vivus.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="js/demos/demo-law-firm.js"></script>	
		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>




		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


	</body>
</html>
