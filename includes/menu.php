<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 98, 'stickySetTop': '-140px', 'stickyChangeLogo': true}">
				<div class="header-body border-color-primary border-top-0 box-shadow-none" id="headerTop">
					<div class="header-container container z-index-2">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="index.php">
											<img alt="" width="179" height="103" id="logoHeader" data-sticky-width="107" data-sticky-height="61" data-sticky-top="117" src="img/demos/law-firm/logo-law-firm.png" style="padding:5px;">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row h-100">
									<ul class="header-extra-info d-flex h-100 align-items-center">
										<li class="align-items-center h-100 py-4">
											<div class="header-extra-info-text h-100 py-2">
												<div class="feature-box feature-box-style-2 align-items-center">
													<div class="feature-box-icon" style="left:0px!important;">
														<i class="far fa-envelope text-7 p-relative"></i>
													</div>
													<div class="feature-box-info pl-1">
														<label style="color:white;">Entre em contato</label>
														<strong><a style="color:white!important;" href="mailto:contato@ribeiroemiranda.adv.br">contato@ribeiroemiranda.adv.br</a></strong>
													</div>
												</div>
											</div>
										</li>
										<li class="align-items-center h-100 py-4">
											<div class="header-extra-info-text h-100 py-2">
												<div class="feature-box feature-box-style-2 align-items-center">
													<div class="feature-box-icon" style="left:0px!important;">
														<i class="fab fa-whatsapp text-7 p-relative"></i>
													</div>
													<div class="feature-box-info pl-1">
														<label style="color:white;">WhatsApp</label>
														<strong><a style="color:white!important;" href="https://api.whatsapp.com/send?phone=5561982121086&text=Ol%C3%A1%2C%20vim%20atrav%C3%A9s%20do%20site%20da%20Ribeiro e Miranda | Advocacia%2C%20e%20desejo%20algumas%20informa%C3%A7%C3%B5es.">(61) 98212-1086</a></strong>
													</div>
												</div>
											</div>
										</li>
										<li class="align-items-center h-100 py-4">
											<div class="header-extra-info-text h-100 py-2">
												<div class="feature-box feature-box-style-2 align-items-center">
													<div class="feature-box-icon" style="left:0px!important;">
														<i class="fas fa-phone text-7 p-relative"></i>
													</div>
													<div class="feature-box-info pl-1">
														<label style="color:white;">Telefone</label>
														<strong style="color:white!important;">(61) 3202-3999</strong>
													</div>
												</div>
											</div>
										</li>										
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="header-nav-bar bg-primary" data-sticky-header-style="{'minResolution': 991}" data-sticky-header-style-active="{'background-color': 'transparent'}" data-sticky-header-style-deactive="{'background-color': '#0088cc'}">
						<div class="container">
							<div class="header-row">
								<div class="header-column">
									<div class="header-row">
										<div class="header-column">
											<div class="header-nav header-nav-stripe header-nav-divisor header-nav-force-light-text justify-content-center" data-sticky-header-style="{'minResolution': 991}" data-sticky-header-style-active="{'margin-left': '150px'}" data-sticky-header-style-deactive="{'margin-left': '0'}">
												<div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
													<nav class="collapse">
														<ul class="nav nav-pills" id="mainNav">
															<li class="dropdown">
																<a class="dropdown-item menu-item active" href="#home">
																	Home
																</a>
															</li>
															<li class="dropdown">
																<a class="dropdown-item menu-item" href="#institucional">
																	Institucional
																</a>
															</li>
															<li class="dropdown">
																<a class="dropdown-item menu-item" href="#equipe">
																	Equipe
																</a>
															</li>
															<li class="dropdown">
																<a class="dropdown-item menu-item" href="#atuacao">
																	Áreas de Atuação
																</a>
															</li>										
															<li class="dropdown">
																<a class="dropdown-item menu-item" href="#contato">
																	Contatos
																</a>
															</li>															
														</ul>
													</nav>
												</div>
											</div>
										</div>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>