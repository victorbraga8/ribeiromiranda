<style type="text/css">
  .embed-responsive {
    position: relative;
    display: block;
    height: 0;
    padding: 0;
    overflow: hidden;
}
</style>
<div class="modal fade" id="ElisabethRibeiro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
<!--       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div> -->
      <div class="modal-body" style="padding:0!important;">
        <div class='embed-responsive' style="padding-bottom:150%">
          <embed src="includes/Teste.pdf#toolbar=0" width='100%' height='100%' type="application/pdf">
        </div>
      </div>
      <div class="modal-footer" style="justify-content: center!important;">
        <button type="button" class="btn btn-default btnModal" data-dismiss="modal" >Fechar</button>
        <a href="includes/Teste.pdf" download><button type="button" class="btn btn-primary btnModal">Baixar Cartão Interativo</button></a>
      </div>
    </div>
  </div>
</div>