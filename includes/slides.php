<div role="main" class="main">
				<div class="slider-container rev_slider_wrapper" style="height: 650px;">
					<div id="revolutionSlider" class="slider rev_slider manual" data-version="5.4.8">
						<ul>
							<li data-transition="fade" data-title="Ribeiro e Miranda" data-thumb="img/demos/law-firm/slides/slide-law-firm-1.jpg">

								<img src="img/demos/law-firm/slides/slide-law-firm-1.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat"
									class="rev-slidebg">

								<div class="tp-caption top-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="-95"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">BEM VINDO A</div>

								<div class="tp-caption main-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="-45"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">RIBEIRO E MIRANDA</div>

								<div class="tp-caption bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="5"
									data-start="2000"
									style="z-index: 5"
									data-transform_in="y:[100%];opacity:0;s:500;">ADVOCACIA.</div>

								<!-- <a class="tp-caption btn btn-primary btn-lg"
									data-hash
									data-hash-offset="85"
									href="#home-intro"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="80"
									data-start="2500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Request a Free Consultation</a> -->
								
							</li>
							<li data-transition="fade" data-title="Área de Atuação" data-thumb="img/demos/law-firm/slides/slide-law-firm-2.jpg">

								<img src="img/demos/law-firm/slides/slide-law-firm-2.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<div class="tp-caption main-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="-205"
									data-start="1000"
									style="z-index: 5; font-size: 40px;"
									data-transform_in="y:[-300%];opacity:0;s:500;">ÁREAS DE ATUAÇÃO</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="-145"
									data-start="1500"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i> Consultoria</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="-100"
									data-start="1800"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i> Mediação e Arbitragem</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="-55"
									data-start="2100"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i> Governança Corporativa (Compliance)</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="-10"
									data-start="2400"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i> Direito Imobiliário e Condominial</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="35"
									data-start="2700"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i> Direito Público e Administrativo</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="80"
									data-start="3000"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i> Direito Ambiental e Agronegócio</div>

								<a class="tp-caption btn btn-primary btn-lg"
									data-hash
									data-hash-offset="85"
									href="#atuacao"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="150"
									data-start="3500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Conheça mais <i class="fas fa-long-arrow-alt-right"></i></a>

							</li>
							<li data-transition="fade" data-title="Nossa Equipe" data-thumb="img/demos/law-firm/slides/slide-law-firm-3.jpg">

								<img src="img/demos/law-firm/slides/slide-law-firm-3.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<div class="tp-caption top-label"
									data-x="left" data-hoffset="100"
									data-y="center" data-voffset="-95"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">EQUIPE EXPERIENTE</div>

								<div class="tp-caption main-label"
									data-x="left" data-hoffset="100"
									data-y="center" data-voffset="-45"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">E QUALIFICADA</div>

								<div class="tp-caption bottom-label"
									data-x="left" data-hoffset="100"
									data-y="center" data-voffset="5"
									data-start="2000"
									style="z-index: 5"
									data-transform_in="y:[100%];opacity:0;s:500;">Possuímos a solução adequada para o seu caso.</div>

								<!-- <a class="tp-caption btn btn-primary btn-lg"
									href="http://themeforest.net/item/porto-responsive-html5-template/4106987"
									data-x="left" data-hoffset="100"
									data-y="center" data-voffset="80"
									data-start="2500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Purchase Now</a> -->

							</li>
						</ul>
					</div>
				</div>