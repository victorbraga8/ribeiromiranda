<?php
	require_once('./includes/head.php');
	require_once('./includes/menu.php');
	require_once('./includes/slides.php');
?>
<!-- 				<section class="section section-default section-no-border mt-0" id="home">
					<div class="container pt-3 pb-4">
						<div class="row justify-content-around">
							<div class="col-lg-7 mb-4 mb-lg-0">
								<h2 class="mb-0">Quem somos</h2>
								<div class="divider divider-primary divider-small mb-4">
									<hr class="mr-auto">
								</div>
								<p class="mt-4">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat.</p>

								<a class="btn btn-outline custom-border-width btn-primary mt-3 mb-2 custom-border-radius font-weight-semibold text-uppercase" href="#">Conheça Mais</a>
							</div>
							<div class="col-lg-4">
								<h4 class="mb-0">Nosso Compromisso</h4>
								<div class="divider divider-primary divider-small mb-4">
									<hr class="mr-auto">
								</div>
								<p class="mt-4 mb-0">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat.</p>
							</div>
						</div>
					</div>
				</section> -->
			<section id="atuacao" class="mb-5">
				<div class="container" id="practice-areas">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h2 class="mt-4 mb-0">Áreas de <strong>Atuação</strong></h2>
							<div class="divider divider-primary divider-small divider-small-center mb-4">
								<hr>
							</div>
						</div>
					</div>

					<div class="row mt-4">
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Consultoria</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Advocacia Preventiva
											</li>
											<li>
												Consultoria Multidisciplinar de Prevenção aos Litígios
											</li>
											<li>
												Consultivo Especializado
											</li>
											<li>
												Relações Governamentais e Assessoria Parlamentar
											</li>
											<li>
												Advocacia de partido-atuação mensal
											</li>
											<li>
												Parecer
											</li>
											<li>
												Análise e Manifestação de Contratos e Propostas 
											</li>											
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Mediação e Arbitragem</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Métodos alternativos e adequados em resolução de conflitos
											</li>
											<li>
												Conciliação
											</li>
											<li>
												Negociação (Mesas redondas-Meeting)
											</li>
											<li>
												Medidas judiciais preparatórias e/ou incidentais ao procedimento arbitral
											</li>
											<li>
												Revisão e assessoria na elaboração e negociação de cláusula de resolução de disputas em contratos complexos
											</li>											
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-4 mt-lg-3">
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Governança Corporativa (Compliance)</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Gestão estratégica e levantamento de riscos
											</li>
											<li>
												Programa de Integridade e Transparência
											</li>
											<li>
												Combate à Corrupção, Lavagem de Dinheiro e Financiamento ao Terrorismo
											</li>
											<li>
												Auditoria e Avaliação de riscos (Due Diligence)
											</li>
											<li>
												Política de Proteção de Dados  
											</li>
											<li>
												Treinamentos internos
											</li>											
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Direito Imobiliário e Condominial</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Elaboração e atualização de Convenção Condominial
											</li>
											<li>
												Elaboração e Atualização de Regimento ou Regulamento Interno
											</li>
											<li>
												Assessoria em Assembleia
											</li>
											<li>
												Cobrança de cotas condominiais
											</li>
											<li>
												Prestação de contas do condomínio
											</li>
											<li>
												Proteção à propriedade, esbulho possessório, construção/incorporações, condomínio, desapropriação, ações renovatórias de locação, revisionais de aluguel, despejo, usucapião
											</li>
											<li>
												Elaboração de Comunicados, Multas, Advertências e Notificações 
											</li>
											<li>
												Operações imobiliárias,  compromissos de compra e venda, permutas, dação em pagamento, comodatos, constituição de hipotecas, alienação fiduciária, locações, condomínios, loteamentos, shoppings centers, centro de distribuição, fábricas/indústrias
											</li>
											<li>
												Alvará de funcionamento, autorizações, habite-se 
											</li>
											<li>
												Imóvel na planta e entrega das chaves
											</li>											
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Direito Público e Administrativo</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Relações Governamentais
											</li>
											<li>
												Assessoria em processos de licitação e procedimentos administrativos em geral e revisão e análise de contratos administrativos
											</li>
											<li>
												Prestação de contas 
											</li>
											<li>
												Processo Administrativo Disciplinar-PAD
											</li>
											<li>
												Acompanhamento de projetos de lei, audiências públicas e debates sobre temas de interesse
											</li>
											<li>
												Consultoria e assessoria junto aos órgãos governamentais, autarquias, agências, entidades de conselho de classe e demais órgãos reguladores
											</li>
											<li>
												Assessoria Parlamentar-ASPAR 
											</li>
											<li>
												Elaboração de pareceres e de memorandos sobre aspectos regulatórios e administrativos
											</li>
											<li>
												Mandado de segurança e outros remédios constitucionais
											</li>											
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Direito Ambiental e Agronegócio</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Consultoria em operações imobiliárias rurais
											</li>
											<li>
												Contratos de natureza agropecuária (parceria rural, arrendamento rural, compra e venda de insumos agrícolas, matérias-primas e commodities)
											</li>
											<li>
												Assessoria à propriedade rural e questões de titularidade e posse
											</li>
											<li>
												Licença ambiental
											</li>
											<li>
												Multa decorrente de infração ambiental
											</li>							
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Direito Digital</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Consultoria especializada em startups brasileiras
											</li>
											<li>
												Análise jurídica de aplicativos (app's) 
											</li>
											<li>
												Assessoria na elaboração e revisão de contratos ligados às atividades de influenciadores digitais.
											</li>
											<li>
												Consultoria jurídica na elaboração de termos de uso, contratos em geral, e outros instrumentos jurídicos, visando regular o uso de serviços e o comércio (e-commerce)
											</li>
											<li>
												Lei Geral de Proteção de Dados
											</li>
											<li>
												Indenização por danos decorrentes de violação de direitos e perseguição (stalking), ocorrida na internet 
											</li>
											<li>
												Direito de Imagem e Direito à Honra nas redes sociais
											</li>										
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Direito do Trabalho e Previdenciário</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Rescisão trabalhista e verbas rescisórias
											</li>
											<li>
												Contrato de trabalho
											</li>
											<li>
												Assédio sexual e moral no trabalho
											</li>
											<li>
												Periculosidade e insalubridade
											</li>
											<li>
												Sindicatos e Associação de trabalhadores
											</li>	
											<li>
												Aposentadoria, Benefícios, Pensão e Seguros
											</li>
											<li>
												Acidentes de trabalho
											</li>																		
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Direito Empresarial</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Consultoria mensal de prevenção aos litígios
											</li>
											<li>
												Elaboração e Alteração de Estatutos, Contratos Sociais e Acordo de Cotistas
											</li>
											<li>
												Títulos de Crédito
											</li>
											<li>
												Extinção, Falência e Recuperação Judicial
											</li>
											<li>
												Franquias
											</li>																			
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Direito de Família e Sucessões</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Inventário
											</li>
											<li>
												Partilha de bens
											</li>
											<li>
												Regime de guarda do menor
											</li>
											<li>
												Alienação Parental
											</li>
											<li>
												Divórcio
											</li>
											<li>
												Pensão alimentícia 
											</li>
											<li>
												Tutela e curatela
											</li>
											<li>
												Prestação de contas
											</li>
											<li>
												Testamento
											</li>																		
										</ul>	
									</div>																	
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-box feature-box-style-2 mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">								
								<div class="row">
								<i class="fas fa-check alinhaIcone"></i>
									<h4 class="mb-2">Direito Civil do Consumidor</h4>										
									<!-- </div> -->									
									<div class="col-sm-12">
										<ul>
											<li>
												Liberdade de escolha e contratos de adesão
											</li>
											<li>
												Venda casada
											</li>
											<li>
												Danos Morais
											</li>
											<li>
												Publicidade abusiva ou enganosa
											</li>
											<li>
												Garantia e troca
											</li>
											<li>
												Negativação indevida em órgãos de proteção ao crédito
											</li>																
										</ul>	
									</div>																	
								</div>
							</div>
						</div>																																					
					</div>					

<!-- 					<div class="row mt-lg-3 mb-4">
						<div class="col-lg-4">
							<div class="feature-box feature-box-style-2 mb-2 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
								<div class="feature-box-icon w-auto mt-3">
									<i class="fas fa-check"></i>
								</div>
								<div class="feature-box-info ml-3">
									<h4 class="mb-2">Divorce Law</h4>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="feature-box feature-box-style-2 mb-2 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
								<div class="feature-box-icon w-auto mt-3">
								<i class="fas fa-check"></i>
								</div>
								<div class="feature-box-info ml-3">
									<h4 class="mb-2">Capital Law</h4>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="feature-box feature-box-style-2 mb-2 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
								<div class="feature-box-icon w-auto mt-3">
									<i class="fas fa-check"></i>
								</div>
								<div class="feature-box-info ml-3">
									<h4 class="mb-2">Accident Law</h4>
							<!-- 		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla.</p> -->
									<!-- <a class="mt-3" href="demo-law-firm-practice-areas-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a> -->
								</div>
							</div>
						</div>
					</div> -->
				</div>
			</section>	
			<div class="row">
				<div class="col-sm-12">
					<iframe id="mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6842.480010850135!2d-47.89944368130369!3d-15.739922540131131!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3a36214fffff%3A0x6464fd795627da2c!2sRibeiro%20Associados!5e0!3m2!1spt-BR!2sbr!4v1629586480573!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>				
				</div>
			</div>
			
				<section class="custom-section-padding mt-5" id="institucional">
					<div class="container">
						<div class="row mb-3">							
							<div class="col-md-6 col-lg-6">
								<h2 class="font-weight-bold text-color-dark"> Ribeiro e Miranda | Advocacia</h2>
								<p>Escritório fundado há quase duas décadas na capital do País, onde a experiência, aliada à prática e ao conhecimento garantem ao cliente o melhor atendimento, sem abrir mão da tecnologia como aliada na gestão de processos.</p>
								<p>
									Referência na defesa aguerrida de suas causas, o escritório acumula cases de sucesso tanto na esfera consultiva -administrativa quanto no âmbito contencioso-judicial, que se destacam pela eficiência e celeridade na condução e resolução do conflito.
								</p>
								<p>
									Ribeiro & Miranda Advocacia, conhecido por buscar soluções estratégicas aos clientes, foi um dos pioneiros a implementar os métodos alternativos às soluções de conflitos, como mediação e arbitragem, possuindo certificação internacional na matéria.
								</p>
								<p class="font-weight-bold">
									"A advocacia não é profissão de covardes"  (Sobral Pinto)
								</p>
<!-- 								<div class="pl-4">
									<div class="row">
										<div class="col-lg-6">
											<ul class="list list-icons list-icons-style-3 list-tertiary">
												<li><i class="fas fa-chevron-right"></i> Certified Professionals</li>
												<li><i class="fas fa-chevron-right"></i> Former Chief Executives</li>
												<li><i class="fas fa-chevron-right"></i> Real Estate Professionals</li>
											</ul>	
										</div>
										<div class="col-lg-6">
											<ul class="list list-icons list-icons-style-3 list-tertiary">
												<li><i class="fas fa-chevron-right"></i> Nobel Laureate Economists</li>
												<li><i class="fas fa-chevron-right"></i> Former Political Leaders</li>
												<li><i class="fas fa-chevron-right"></i> Chartered Financial Analysts</li>
											</ul>
										</div>
									</div>
									<a class="btn btn-outline custom-border-width btn-primary mt-3 mb-2 custom-border-radius font-weight-semibold text-uppercase" href="#">Conheça Mais</a>
								</div> -->
							</div>
							<div class="col-lg-6">
								<h2 class="font-weight-bold text-color-dark"> Institucional</h2>
								<div class="toggle toggle-minimal toggle-primary" style="margin:-12px 0 20px!important;" data-plugin-toggle data-plugin-options="{ 'isAccordion': true }">
								<section class="toggle">
									<label>Nossa Missão</label>
									<div class="toggle-content">
										<p>Prestar à sociedade um atendimento jurídico diferenciado e eficiente, sempre em constante atualização, com foco na aplicação do Direito em busca da Justiça.</p>
									</div>
								</section>
								<section class="toggle">
									<label>Nossa Visão</label>
									<div class="toggle-content">
										<p>Ser referência no atendimento ao cliente com o reconhecimento pela realização de uma advocacia de qualidade e confiança, contribuindo para o aperfeiçoamento da Justiça e Pacificação Social.</p>
									</div>
								</section>
								<section class="toggle">
									<label>Nossos Valores</label>
									<div class="toggle-content">
										<p>Exercer a vocação profissional com proatividade, ética, equidade, excelência e respeito.</p>
									</div>
								</section>
							</div>
							</div>
						</div>
					</div>
				</section>
			
				<!-- <div class="container-fluid">
					<div class="row">
						<div class="col-lg-6 p-0">
							<section class="section section-primary match-height border-0" style="background-image: url(img/patterns/fancy.jpg);">
								<div class="row justify-content-end ml-lg-5">
									<div class="col-half-section col-half-section-right">
										<h2 class="mb-0">Depoimentos</h2>
										<div class="divider divider-light divider-small mb-4">
											<hr class="mr-auto">
										</div>

										<div class="owl-carousel owl-theme mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': false, 'dots': true}">
											<div>
												<div class="testimonial testimonial-style-3 testimonial-trasnparent-background testimonial-alternarive-font">
													<blockquote class="text-light">
														<p class="text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Phasellus semper scelerisque purus, et semper nisl lacinia sit amet. Praesent venenatis turpis vitae purus semper, eget sagittis velit venenatis.</p>
													</blockquote>
													<div class="testimonial-author">
														<div class="testimonial-author-thumbnail">
															<img src="img/clients/client-1.jpg" class="img-fluid rounded-circle" alt="">
														</div>
														<p><strong>John Smith</strong><span class="text-light">CEO & Founder - Okler</span></p>
													</div>
												</div>
											</div>
											<div>
												<div class="testimonial testimonial-style-3 testimonial-trasnparent-background testimonial-alternarive-font">
													<blockquote class="text-light">
														<p class="text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Phasellus semper scelerisque purus, et semper nisl lacinia sit amet.</p>
													</blockquote>
													<div class="testimonial-author">
														<div class="testimonial-author-thumbnail">
															<img src="img/clients/client-2.jpg" class="img-fluid rounded-circle" alt="">
														</div>
														<p><strong>Jessica Smith</strong><span class="text-light">Marketing - Okler</span></p>
													</div>
												</div>
											</div>
											<div>
												<div class="testimonial testimonial-style-3 testimonial-trasnparent-background testimonial-alternarive-font">
													<blockquote class="text-light">
														<p class="text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Phasellus semper scelerisque purus, et semper nisl lacinia sit amet. Praesent venenatis turpis vitae purus semper, eget sagittis velit venenatis.</p>
													</blockquote>
													<div class="testimonial-author">
														<div class="testimonial-author-thumbnail">
															<img src="img/clients/client-3.jpg" class="img-fluid rounded-circle" alt="">
														</div>
														<p><strong>Bob Smith</strong><span class="text-light">COO - Okler</span></p>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</section>
						</div>
						<div class="col-lg-6 p-0 visible-md visible-lg">
							<section class="parallax section section-parallax match-height" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="img/demos/law-firm/parallax/parallax-law-firm.jpg" style="min-height: 450px;">
							</section>
						</div>
					</div>-->

				<section id="equipe">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<h2 class="mt-4 mb-0">Ribeiro e Miranda | <strong>Sócias</strong></h2>
								<div class="divider divider-primary divider-small divider-small-center mb-4">
									<hr>
								</div>
							</div>
						</div>
						<div class="row mt-4">
							<div class="owl-carousel owl-theme owl-team-custom show-nav-title mb-0" data-plugin-options="{'items': 2, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'center': true}">
							<div class="text-center mb-4">
									<img src="img/team/team-23.jpg" class="img-fluid" alt="">
									<h4 class="mt-3 mb-0">Elisabeth Ribeiro</h4>
									<p class="mb-0">Advogada</p>
								<span class="thumb-info-social-icons mt-2 pb-0">
									<span data-toggle="tooltip" data-placement="top" title="Enviar E-mail">
										<a href="mailto:mail@example.com"><i class="far fa-envelope"></i><span>Email</span></a>
									</span>

									<span data-toggle="tooltip" data-placement="top" title="Acessar Cartão Virtual" class="toolTipRM">
										<a href="#ElisabethRibeiro" data-toggle="modal"><i class="fas fa-user"></i><span>Cartão 
										Virtual</span></a>
									</span>
								</span>
							</div>
							<div class="text-center mb-4">
									<img src="img/team/team-23.jpg" class="img-fluid" alt="">
									<h4 class="mt-3 mb-0">Gabriella Miranda</h4>
									<p class="mb-0">Advogada</p>
								<span class="thumb-info-social-icons mt-2 pb-0">
									<span data-toggle="tooltip" data-placement="top" title="Enviar E-mail">
										<a href="mailto:mail@example.com"><i class="far fa-envelope"></i><span>Email</span></a>
									</span>

									<span data-toggle="tooltip" data-placement="top" title="Acessar Cartão Virtual" class="toolTipRM">
										<a href="#ElisabethRibeiro" data-toggle="modal"><i class="fas fa-user"></i><span>Cartão 
										Virtual</span></a>
									</span>
								</span>
							</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 text-center">
								<h2 class="mt-4 mb-0">Ribeiro e Miranda | <strong>Equipe</strong></h2>
								<div class="divider divider-primary divider-small divider-small-center mb-4">
									<hr>
								</div>
							</div>
						</div>						
						<div class="row mt-4" style="text-align: center;">
							<div class="col-sm-4">
								<h4 class="mt-3 mb-0">Integrante 1</h4>
								<p class="mb-0">Advogada</p>
								<span class="thumb-info-social-icons mt-2 pb-0">
									<a href="mailto:mail@example.com"><i class="far fa-envelope"></i><span>Email</span></a>
								</p>
							</div>
							<div class="col-sm-4">
								<h4 class="mt-3 mb-0">Integrante 2</h4>
								<p class="mb-0">Advogada</p>
								<span class="thumb-info-social-icons mt-2 pb-0">
									<a href="mailto:mail@example.com"><i class="far fa-envelope"></i><span>Email</span></a>
								</p>
							</div>
							<div class="col-sm-4">
								<h4 class="mt-3 mb-0">Integrante 3</h4>
								<p class="mb-0">Advogada</p>
								<span class="thumb-info-social-icons mt-2 pb-0">
									<a href="mailto:mail@example.com"><i class="far fa-envelope"></i><span>Email</span></a>
								</p>
							</div>
						</div>
					</div>
				</section>
<?php 
				require_once("includes/cartao-elisabeth.php");
?>					
				<!-- </div>  -->

				<section class="parallax section section-text-light section-parallax section-center mt-5" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/demos/law-firm/parallax/parallax-law-firm-2.jpg">
					<div class="container">
						<div class="row counters counters-text-light">
							<div class="col-lg-4 col-sm-6">
								<div class="counter mb-4 mt-4">
									<i class="icon-user-following icons"></i>
									<strong data-to="300" data-append="+">0</strong>
									<label>Atendimentos</label>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6">
								<div class="counter mb-4 mt-4">
									<i class="icon-diamond icons"></i>
									<strong data-to="15">0</strong>
									<label>Anos em Atuação</label>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6">
								<div class="counter mb-4 mt-4">
									<i class="icon-paper-plane icons"></i>
									<strong data-to="190">0</strong>
									<label>Casos de Sucesso</label>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="parceiros">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<h2 class="mt-4 mb-0">Ribeiro e Miranda | <strong>Parceiros</strong></h2>
								<div class="divider divider-primary divider-small divider-small-center mb-4">
									<hr>
								</div>
							</div>
						</div>											
						<div class="row mt-4">
							<div class="col-lg-5">
								<h5 class="text-uppercase mt-4">Nome do Parceiro 1</h5>
								<span class="thumb-info thumb-info-side-image thumb-info-no-zoom">
									<span class="thumb-info-side-image-wrapper">
										<img src="img/blog/square/blog-23.jpg" class="img-fluid" alt="" style="width: 200px;">
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">
											<h5 class="text-uppercase mb-1">Dados do Parceiro</h5>
											<ul style="list-style-type: none;">
												<li>
													Endereço:
												</li>
												<li>
													Telefone:
												</li>
												<li>
													Email:
												</li>
											</ul>
										</span>
									</span>
								</span>
							</div>
							<div class="col-lg-7 mg-t55">
								<!-- <h5 class="text-uppercase mt-4">Nome do Parceiro 2</h5> -->
								<span class="thumb-info thumb-info-side-image thumb-info-no-zoom">
									<span class="thumb-info-side-image-wrapper">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3840.2393934855972!2d-47.90239498572312!3d-15.738470125773368!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3a36214fffff%3A0x6464fd795627da2c!2sRibeiro%20Associados!5e0!3m2!1spt-BR!2sbr!4v1633046102710!5m2!1spt-BR!2sbr" width="635" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
									</span>
								</span>
							</div>
							<div class="col-lg-5">
								<h5 class="text-uppercase mt-4">Nome do Parceiro 2</h5>
								<span class="thumb-info thumb-info-side-image thumb-info-no-zoom">
									<span class="thumb-info-side-image-wrapper">
										<img src="img/blog/square/blog-23.jpg" class="img-fluid" alt="" style="width: 200px;">
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">
											<h5 class="text-uppercase mb-1">Dados do Parceiro</h5>
											<ul style="list-style-type: none;">
												<li>
													Endereço:
												</li>
												<li>
													Telefone:
												</li>
												<li>
													Email:
												</li>
											</ul>
										</span>
									</span>
								</span>
							</div>
							<div class="col-lg-7 mg-t55">
								<!-- <h5 class="text-uppercase mt-4">Nome do Parceiro 2</h5> -->
								<span class="thumb-info thumb-info-side-image thumb-info-no-zoom">
									<span class="thumb-info-side-image-wrapper">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3840.2393934855972!2d-47.90239498572312!3d-15.738470125773368!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3a36214fffff%3A0x6464fd795627da2c!2sRibeiro%20Associados!5e0!3m2!1spt-BR!2sbr!4v1633046102710!5m2!1spt-BR!2sbr" width="635" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
									</span>
								</span>
							</div>																		
						</div>

					</div>
				</section>
				<!-- <div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h2 class="mt-4 mb-0">Latest News</h2>
							<div class="divider divider-primary divider-small divider-small-center mb-4">
								<hr>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">

							<span class="thumb-info thumb-info-side-image thumb-info-no-zoom border mb-5">
								<span class="thumb-info-side-image-wrapper p-0 d-none d-sm-block">
									<a title="" href="demo-law-firm-news-detail.html">
										<img src="img/demos/law-firm/blog/blog-law-firm-1.jpg" class="img-fluid" alt="" style="width: 195px;">
									</a>
								</span>
								<span class="thumb-info-caption">
									<span class="thumb-info-caption-text py-0 px-4">
										<h2 class="mb-3 mt-3"><a title="" class="text-dark" href="demo-law-firm-news-detail.html">Award of Honor</a></h2>
										<span class="post-meta">
											<span>January 10, 2017 | <a href="#">John Doe</a></span>
										</span>
										<p class="text-3 pt-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
										<a class="mt-3" href="demo-law-firm-news-detail.html">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
									</span>
								</span>
							</span>

						</div>
						<div class="col-lg-6">

							<span class="thumb-info thumb-info-side-image thumb-info-no-zoom border mb-5">
								<span class="thumb-info-side-image-wrapper p-0 d-none d-sm-block">
									<a title="" href="demo-law-firm-news-detail.html">
										<img src="img/demos/law-firm/blog/blog-law-firm-2.jpg" class="img-fluid" alt="" style="width: 195px;">
									</a>
								</span>
								<span class="thumb-info-caption">
									<span class="thumb-info-caption-text py-0 px-4">
										<h2 class="mb-3 mt-3"><a title="" class="text-dark" href="demo-law-firm-news-detail.html">The Best Lawyer</a></h2>
										<span class="post-meta">
											<span>January 10, 2017 | <a href="#">John Doe</a></span>
										</span>
										<p class="text-3 pt-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
										<a class="mt-3" href="demo-law-firm-news-detail.html">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
									</span>
								</span>
							</span>

						</div>
					</div>
				</div> -->
				<div class="container">
					<div class="row text-center pt-4 mt-5">
						<div class="col">
							<h2 class="font-weight-bold text-8 mb-2">
								<span>Convênios</span>								
							</h2>
							<h4 class="text-primary lead tall text-4">Texto sobre apresentação de convênios.</h4>
						</div>
					</div>

					<div class="row text-center mt-5 pb-5 mb-5">
						<div class="col-sm-2 pd-5 display-flex">
							<img class="responsive img-convenio" src="img/logos/logo-6.png">
						</div>
						<div class="col-sm-2 pd-5 display-flex">
							<img class="responsive img-convenio" src="img/logos/logo-7.png">
						</div>
						<div class="col-sm-2 pd-5 display-flex">
							<img class="responsive img-convenio" src="img/logos/logo-6.png">
						</div>
						<div class="col-sm-2 pd-5 display-flex">
							<img class="responsive img-convenio" src="img/logos/logo-7.png">
						</div>
						<div class="col-sm-2 pd-5 display-flex">
							<img class="responsive img-convenio" src="img/logos/logo-6.png">
						</div>
						<div class="col-sm-2 pd-5 display-flex">
							<img class="responsive img-convenio" src="img/logos/logo-7.png">
						</div>
					</div>
				</div>				

				<section class="section section-background section-footer" id="contato" style="background-image: url(img/demos/law-firm/contact/contact-background.jpg); background-position: 50% 100%; background-size: cover;">
					<div class="container">
						<div class="row justify-content-end">
							<div class="col-lg-6">
								<h2 class="mt-5 mb-0">Entre em Contato</h2>
								<p>Entre em contato com o nosso time.</p>
								<div class="divider divider-primary divider-small mb-4">
									<hr class="mr-auto">
								</div>
								<form id="contactForm" class="contact-form" action="php/contact-form.php" method="POST">
									<div class="form-row">
										<div class="form-group col-sm-6">
											<input type="text" value="" placeholder="Informe o seu nome" data-msg-required="Por favor informe o seu nome." maxlength="100" class="form-control" name="name" id="name" required>
										</div>
										<div class="form-group col-sm-6">
											<input type="email" value="" placeholder="Informe o seu e-mail" data-msg-required="Por favor informe o seu e-mail." data-msg-email="Insira um e-mail válido." maxlength="100" class="form-control" name="email" id="email" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<input type="text" value="" placeholder="Assunto" data-msg-required="Por favor informe o assunto." maxlength="100" class="form-control" name="subject" id="subject" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<textarea maxlength="5000" placeholder="Mensagem" data-msg-required="Por favor digite a sua mensagem." rows="3" class="form-control" name="message" id="message" required></textarea>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<input type="button" value="Enviar" class="btn btn-primary mb-5" data-loading-text="Loading...">

											<div class="contact-form-success alert alert-success d-none" id="contactSuccess">
												Mensagem enviada com sucesso.
											</div>

											<div class="contact-form-error alert alert-danger d-none" id="contactError">
												Ocorreu um erro, tente novamente mais tarde.
												<span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>
					</div>
				</section>
			</div>

<?php
	require_once('./includes/footer.php');
?>