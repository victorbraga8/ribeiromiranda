logoHeader = document.getElementById('logoHeader');

const config = {attributes: true, attributesFilter:['style']};

const callback = (mutationList, observer)=>{
    for(const mutation of mutationList){
        if(mutation.attributeName == 'style'){
            if(logoHeader.style.height == '61px'){
                logoHeader.setAttribute('src','img/demos/law-firm/logo-sticky.png');                
            }else{
                logoHeader.setAttribute('src','img/demos/law-firm/logo-law-firm.png')
            }           
        }
    }
}

btnMenu = document.getElementsByClassName('menu-item');

function verificaClasse(){
    for(i=0; i<btnMenu.length; i++){    
        if(btnMenu[i].classList.contains('active')){
            if(btnMenu[i].href && this.href){
                btnMenu[i].classList.remove('active');
                this.classList.add('active');
            }
        }
    }       
}

for(i=0; i<btnMenu.length; i++){
    btnMenu[i].addEventListener('click',verificaClasse);    
}

const observer = new MutationObserver(callback);
observer.observe(logoHeader, config);

setTimeout(()=>{
    owlStage = document.querySelector('.owl-stage');
    owlStage.style.transform = "translate3d(300px, 0px, 0px)!important;"    
},1500)

btnModal = document.getElementsByClassName('btnModal');
    for(i=0; i<btnModal.length; i++){
        btnModal[i].addEventListener('click',removeToolTip);
    }

function removeToolTip(){
    setTimeout(()=>{
        toolTip = document.getElementsByClassName('toolTipRM');
        for(i=0; i<toolTip.length; i++){
            toolTip[i].removeAttribute('aria-describedby');
        }
    },1000)    
}


// src="img/demos/law-firm/logo-law-firm.png"